'use strict';

const MongoClient = require('mongodb').MongoClient;
const puppeteer = require('puppeteer');
const Url = require('url-parse');

/**
 * screenshots page at given url in fullscreen mode
 *
 * @param {string} customerName -
 * @param {puppeteer.Page} Page -
 *
 * @return {int} -
 */
const takeScreenie = async (customerName, page) => {
	await page.screenshot({
		path: `data/customers/${customerName}/screenshot/screenshot.png`,
		fullPage: true,
	});
	return 0;
};
/**
 * extracts all a-tags from given url
 * @param {puppeteer.Page} Page -
 *
 * @return {string[]} -
 */
const extract = async page => {
	const resultsSelector = 'a';
	await page.waitForSelector(resultsSelector).catch(err => {
		console.log(err);
		return [];
	});
	return await page.evaluate(resultsSelector => {
		const anchors = Array.from(document.querySelectorAll(resultsSelector));
		return anchors.map(anchor => anchor.href);
	}, resultsSelector);
};

/**
 * checks if todo is already done
 *
 * @param {string} todo -
 * @param {string[]} dones -
 *
 * @return {boolean} -
 */
const isDone = (todo, dones) => {
	return dones.includes(todo);
};

/**
 * filter what we want
 *
 * @param {string[]} hrefs -
 *
 * @return {string[]} -
 */
const getTargets = hrefs => {
	return [
		...new Set(
			hrefs
				.map(href => {
					const url = Url(href);
					// only ch-domains for the moment
					if (url.origin && url.hostname.split('.').pop() === 'ch')
						return url.origin + '/';
				})
				.filter(href => href)
		),
	];
};

/**
 * add new todos
 *
 * @param {string[]} newTodos -
 * @param {string[]} todos -
 * @param {string[]} dones -
 *
 * @return -
 */
const addTodos = (newTodos, todos, dones) => {
	newTodos = newTodos.filter(
		newTodo => !todos.includes(newTodo) && !isDone(newTodo, dones)
	);
	Array.prototype.push.apply(todos, newTodos);
	console.log(
		`${newTodos.length} todos added, ${todos.length} total todos now`
	);
};
/**
 * scrapes given url for content
 *
 * @param {string} customerName -
 * @param {string} customerUrl -
 * @param {f()} cb -
 *
 * @return -
 */
const scrape = async (customerName, customerUrl, cb) => {
	const browser = await puppeteer.launch();
	const page = await browser.newPage();

	const todos = [customerUrl];
	const dones = [];
	while (todos.length) {
		const todo = todos.pop();
		console.log(`executing todo {${todo}}`);
		// https://pptr.dev/#?product=Puppeteer&version=v1.17.0&show=api-class-page
		await page.goto(todo, { waitUntil: 'networkidle0' }).catch(err => {
			console.log(err);
		});
		const hrefs = getTargets(await extract(page));
		dones.push(todo);
		addTodos(hrefs, todos, dones);

		if (false) takeScreenie();
	}
	await browser.close();
	console.log('browser closed');
	cb();
};

/**
 * main wrapper
 *
 * @param -
 *
 * @return -
 */
const main = () => {
	const mongoUrl = 'mongodb://localhost/boxrec';

	const customerName = 'boxrec';
	const customerUrl = 'https://www.blick.ch/';

	MongoClient.connect(mongoUrl, { useNewUrlParser: true }, (err, db) => {
		console.log('mongo is now connected and ready to use');
		console.log(
			`starting scrape of customer ${customerName} @${customerUrl}`
		);
		scrape(customerName, customerUrl, (err, blaa) => {
			console.log(`extraction done for ${customerUrl}`);
			db.close();
			console.log('mongo is now closed!');
		});
	});
};

// executed if called as main (node src/scraper)
if (require.main === module) {
	main();
}
