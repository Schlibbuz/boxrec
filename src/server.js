const path = require('path');

const bodyParser = require('body-parser');
const express = require('express');
//const expressSession = require('express-session');
const passport = require('passport'),
	LocalStrategy = require('passport-local');

const PORT = process.env.PORT || 5000;

passport.use(
	new LocalStrategy(function(username, password, done) {
		User.findOne({ username: username }, function(err, user) {
			if (err) {
				return done(err);
			}
			if (!user) {
				return done(null, false, { message: 'Incorrect username.' });
			}
			if (!user.validPassword(password)) {
				return done(null, false, { message: 'Incorrect password.' });
			}
			return done(null, user);
		});
	})
);

app = express()
	.use(express.static(path.join(__dirname, '../public')))
	.use(session({ secret: 'cats' }))
	.use(bodyParser.urlencoded({ extended: false }))
	.use(passport.initialize())
	.use(passport.session())
	.set('views', path.join(__dirname, '../views'))
	.set('view engine', 'ejs');

app.get('/', (req, res) => res.render('pages/index'));

app.listen(PORT, () => console.log(`Listening on ${PORT}`));
