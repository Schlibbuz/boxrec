'use strict';

const MongoClient = require('mongodb').MongoClient;
const MongoError = require('mongodb').MongoError;
const puppeteer = require('puppeteer');
const Url = require('url-parse');

/**
 * compose url from todo-Object
 *
 * @param {dict} todo -
 *
 * @return {string} -
 */
const composeUrl = todo => {
	return `${todo.protocol}//${todo.subDomain}.${todo.domain}/`;
};
/**
 * main wrapper
 *
 * @param -
 *
 * @return -
 */
const main = () => {
	const mongoUrl = 'mongodb://localhost';
	const dbName = 'boxrec';

	const startingPoint = {
		protocol: 'https:',
		subDomain: 'www',
		domain: 'blick.ch',
	};

	(async () => {
		const client = new MongoClient(mongoUrl, { useNewUrlParser: true });

		const browser = await puppeteer.launch();
		console.log('browser launched');

		try {
			await client.connect();
			console.log('Connected to mongo');
			const collection = client.db(dbName).collection('scrapes');

			const page = await browser.newPage();

			const todos = [startingPoint];
			while (todos.length) {
				const todo = todos.pop();
				try {
					console.log(composeUrl(todo));
					const response = await page.goto(composeUrl(todo), {
						waitUntil: 'networkidle0',
					});
					console.dir(response);
					const resultsSelector = 'a';
					await page.waitForSelector(resultsSelector);
					const wantedKeys = ['host', 'protocol'];
					const results = [
						...new Set(
							await page.evaluate(resultsSelector => {
								const anchors = Array.from(
									document.querySelectorAll(resultsSelector)
								);
								return anchors.map(anchor => {
									return anchor.href;
								});
							}, resultsSelector)
						),
					].map(result => {
						const parsedUrl = new Url(result);
						for (const key of Object.keys(parsedUrl)) {
							if (wantedKeys.includes(key)) continue;
							delete parsedUrl[key];
						}

						const hostParts = parsedUrl.host.split('.');
						const tld = hostParts.pop();
						const dn = hostParts.pop();
						parsedUrl.domain = [dn, tld].join('.');
						parsedUrl.subDomain = hostParts.join('.');
						parsedUrl.status = 'pending';
						console.log(parsedUrl);
						return parsedUrl;
					});

					try {
						await collection.insertMany(results, {
							ordered: false,
						});
					} catch (err) {
						// only handle MongoError 11000 here (dup-key-violation)
						if (!(err instanceof MongoError && err.code === 11000))
							throw err;

						console.dir(err);
						const numErrs = err.writeErrors.length;
						if (numErrs) {
							if (numErrs === 1) {
								console.log('1 dup-key violation');
							} else {
								console.log(`${numErrs} dup-key violations`);
							}
						}
					}

					const cursor = collection.find();

					while (await cursor.hasNext()) {
						const scrape = await cursor.next();
						console.log(scrape.domain);
					}
				} catch (err) {
					if (err.message.startsWith('net::ERR_NAME_NOT_RESOLVED')) {
						console.log('dns-error');
					} else if (err.message.startsWith('net::ERR_ABORTED')) {
						console.log('protocol-error');
					} else {
						throw err;
					}
				}
				const newTodo = await collection.findOneAndUpdate(
					{ status: 'pending' },
					{ $set: { status: 'processing' } }
				);
				if (newTodo.value) {
					todos.push(newTodo.value);
				}
			}
		} catch (err) {
			console.dir(err);
		}
		await browser.close();
		console.log('browser closed');
		// Close connection
		client.close();
		console.log('mongo closed');
	})();
};

// executed if called as main (node src/scraper)
if (require.main === module) {
	main();
}
